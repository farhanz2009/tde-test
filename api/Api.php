<?php 
require_once(__DIR__.'/../App.php');
require_once(__DIR__.'/../config.php');
/**
* All base API methods
*/
class Api
{
	protected $curl;
	protected $apiKey;

	function __construct()
	{
		$this->curl = curl_init();
		$this->apiKey = constant('API_KEY');
	}

	function apiRequest($args)
	{
		$query = $this->build_query($args);
		$request_url = 'http://ws.audioscrobbler.com/2.0/?'.$query;
		//Set the URL for GET request
		curl_setopt($this->curl, CURLOPT_URL, $request_url);
		//Set transfer to get data as avariable
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		$resp = curl_exec($this->curl);

		// Close request
		curl_close($this->curl);
		return $resp;
	}

	function build_query($args)
	{
		$args['api_key'] = $this->apiKey;
		$args['format'] = 'json';
		$query = http_build_query($args);
		return $query;
	}
}
?>