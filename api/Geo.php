<?php 
require_once('Api.php');
/**
* Fetch top artists by country
*/
class Geo extends Api
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function topArtistsByCountry($country)
	{
		$args['country'] = $country; 
		$args['method'] = 'geo.gettopartists';
 		return $this->apiRequest($args);
	}
}
?>